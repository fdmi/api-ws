<?php
namespace Fdmi\Api2solr\Models;

class Search
{
    public $res;
    public $uri;
    public $url;
    public $client;
    public $q = NULL;
    public $qparams = NULL;
    protected $fl = 'id repository_name:header.repository_name datestamp:header.datestamp response_date:header.response_date title:metadata.title relation:metadata.relation creator:metadata.creator subject:metadata.subject date:metadata.date type:metadata.type format:metadata.format language:metadata.language identifier:metadata.identifier description:metadata.description';
    public $is_all = FALSE;

    public function __construct($request)
    {
        $this->setParams($request);
        $this->setClient();
        $this->setQparams();
        #echo "\n$this->qparams\n"; #DEBUG
        $response = $this->client->request('GET', '/solr/dublincore/select'.$this->qparams);
        $content = $response->getBody()->getContents();
        $this->res = $content;
    }

    public function getRes()
    {
        if ($_ENV['MODE'] != 'development') {
            $tmp_res = (array) json_decode($this->res);
            $tmp_res['responseHeader'] = (array) $tmp_res['responseHeader'];
            unset($tmp_res['responseHeader']['params']);
            return json_encode($tmp_res);
        } else {
            return $this->res;
        }
    }

    public function setParams ($request)
    {
        $rk['id'] = $request->get['id'];
        $vid = new \Valitron\Validator($rk);
        $vid->rule('required', ['id']);
        if($vid->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'id:"'.$rk['id'].'"';
            } else {
                $this->q .= ' id:"'.$rk['id'].'"';
            }
        }

        $rk['reponame'] = $request->get['reponame'];
        $vreponame = new \Valitron\Validator($rk);
        $vreponame->rule('required', ['reponame']);
        if($vreponame->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.repository_name:'.$rk['reponame'];
            } else {
                $this->q .= ' header.repository_name:'.$rk['reponame'];
            }
        }

        $rk['reponame_s'] = $request->get['reponame_s'];
        $vreponame_s = new \Valitron\Validator($rk);
        $vreponame_s->rule('required', ['reponame_s']);
        if($vreponame_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.repository_name_s:"'.$rk['reponame_s'].'"';
            } else {
                $this->q .= ' header.repository_name_s:"'.$rk['reponame_s'].'"';
            }
        }

        $rk['allfields'] = $request->get['allfields'];
        $vallfields = new \Valitron\Validator($rk);
        $vallfields->rule('required', ['allfields']);
        if($vallfields->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= $rk['allfields'];
            } else {
                $this->q .= ' '.$rk['allfields'];
            }
        }

        $rk['title'] = $request->get['title'];
        $vtitle = new \Valitron\Validator($rk);
        $vtitle->rule('required', ['title']);
        if($vtitle->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.title:'.$rk['title'];
            } else {
                $this->q .= ' metadata.title:'.$rk['title'];
            }
        }

        $rk['creator'] = $request->get['creator'];
        $vcreator = new \Valitron\Validator($rk);
        $vcreator->rule('required', ['creator']);
        if($vcreator->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.creator:'.$rk['creator'];
            } else {
                $this->q .= ' metadata.creator:'.$rk['creator'];
            }
        }

        $rk['creator_s'] = $request->get['creator_s'];
        $vcreator_s = new \Valitron\Validator($rk);
        $vcreator_s->rule('required', ['creator_s']);
        if($vcreator_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.creator_s:"'.$rk['creator_s'].'"';
            } else {
                $this->q .= ' metadata.creator_s:"'.$rk['creator_s'].'"';
            }
        }

        # SUBJECT
        $rk['subject'] = $request->get['subject'];
        $vsubject = new \Valitron\Validator($rk);
        $vsubject->rule('required', ['subject']);
        if($vsubject->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.subject:'.$rk['subject'];
            } else {
                $this->q .= ' metadata.subject:'.$rk['subject'];
            }
        }

        $rk['subject_s'] = $request->get['subject_s'];
        $vsubject_s = new \Valitron\Validator($rk);
        $vsubject_s->rule('required', ['subject_s']);
        if($vsubject_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.subject_s:"'.$rk['subject_s'].'"';
            } else {
                $this->q .= ' metadata.subject_s:"'.$rk['subject_s'].'"';
            }
        }

        # TYPE
        $rk['type_s'] = $request->get['type_s'];
        $vtype_s = new \Valitron\Validator($rk);
        $vtype_s->rule('required', ['type_s']);
        if($vtype_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.type_s:"'.$rk['type_s'].'"';
            } else {
                $this->q .= ' metadata.type_s:"'.$rk['type_s'].'"';
            }
        }

        # FORMAT
        $rk['format_s'] = $request->get['format_s'];
        $vformat_s = new \Valitron\Validator($rk);
        $vformat_s->rule('required', ['format_s']);
        if($vformat_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.format_s:"'.$rk['format_s'].'"';
            } else {
                $this->q .= ' metadata.format_s:"'.$rk['format_s'].'"';
            }
        }

        # LANGUAGE
        $rk['language_s'] = $request->get['language_s'];
        $vlanguage_s = new \Valitron\Validator($rk);
        $vlanguage_s->rule('required', ['language_s']);
        if($vlanguage_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'metadata.language_s:"'.$rk['language_s'].'"';
            } else {
                $this->q .= ' metadata.language_s:"'.$rk['language_s'].'"';
            }
        }

        # INSTITUTION
        $rk['institution'] = $request->get['institution'];
        $vinstitution = new \Valitron\Validator($rk);
        $vinstitution->rule('required', ['institution']);
        if($vinstitution->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.institution:'.$rk['institution'];
            } else {
                $this->q .= ' header.institution:'.$rk['institution'];
            }
        }
        
        $rk['institution_s'] = $request->get['institution_s'];
        $vinstitution_s = new \Valitron\Validator($rk);
        $vinstitution_s->rule('required', ['institution_s']);
        if($vinstitution_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.institution_s:"'.$rk['institution_s'].'"';
            } else {
                $this->q .= ' header.institution_s:"'.$rk['institution_s'].'"';
            }
        }

        # LIBRARY
        $rk['library'] = $request->get['library'];
        $vlibrary = new \Valitron\Validator($rk);
        $vlibrary->rule('required', ['library']);
        if($vlibrary->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.library:'.$rk['library'];
            } else {
                $this->q .= ' header.library:'.$rk['library'];
            }
        }

        $rk['library_s'] = $request->get['library_s'];
        $vlibrary_s = new \Valitron\Validator($rk);
        $vlibrary_s->rule('required', ['library_s']);
        if($vlibrary_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.library_s:"'.$rk['library_s'].'"';
            } else {
                $this->q .= ' header.library_s:"'.$rk['library_s'].'"';
            }
        }

        # COLLECTION
        $rk['collection'] = $request->get['collection'];
        $vcollection = new \Valitron\Validator($rk);
        $vcollection->rule('required', ['collection']);
        if($vcollection->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.collection:'.$rk['collection'];
            } else {
                $this->q .= ' header.collection:'.$rk['collection'];
            }
        }

        $rk['collection_s'] = $request->get['collection_s'];
        $vcollection_s = new \Valitron\Validator($rk);
        $vcollection_s->rule('required', ['collection_s']);
        if($vcollection_s->validate()) {
            $this->is_all = TRUE;
            if (is_null($this->q)) {
                $this->q .= 'header.collection_s:"'.$rk['collection_s'].'"';
            } else {
                $this->q .= ' header.collection_s:"'.$rk['collection_s'].'"';
            }
        }

        if (!$this->is_all) {
            $this->q = '*:*';
        }

    }

    public function setClient()
    {
        $this->uri = 'http://'.$_ENV['SOLR_HOST'].':'.$_ENV['SOLR_PORT'];
        $this->url = ['base_uri' => $this->uri, 'timeout'  => 2.0,];
        $this->client = new \GuzzleHttp\Client($this->url);
    }

    public function setQparams()
    {
        $this->qparams .= '?q='.$this->q;
        $this->qparams .= '&indent=true';
        $this->qparams .= '&q.op=AND';
        $this->qparams .= '&useParams=';
        $this->qparams .= '&facet=true';
        $this->qparams .= '&facet.limit=10';
        $this->qparams .= '&facet.field=metadata.creator_s';
        $this->qparams .= '&facet.field=metadata.subject_s';
        $this->qparams .= '&fl=id repository_name:header.repository_name datestamp:header.datestamp response_date:header.response_date title:metadata.title relation:metadata.relation creator:metadata.creator subject:metadata.subject date:metadata.date type:metadata.type format:metadata.format language:metadata.language identifier:metadata.identifier description:metadata.description';
    }

}