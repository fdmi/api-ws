<?php
namespace Fdmi\Api2solr\Views;

class ViewJson
{
    public function __construct($response, $data, $status=true)
    {
        $this->getView($response, $data, $status);
    }

    public function getView($response, $data, $status='200')
    {

        #if ($status == '200') {
        #    $_res = json_encode($data);
        #}
        $response->status($status);
        $response->header("Content-Type", "application/json");
        if ($status == '200') {
            $_res = json_encode($data);
            $response->end($_res);
        }
    }



}