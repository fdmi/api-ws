<?php
require_once 'vendor/autoload.php';

$cnf = new \Symfony\Component\Dotenv\Dotenv();
$cnf->load(__DIR__.'/.env', __DIR__.'/.env.local');

#error_reporting(E_ALL);
ini_set("display_errors", 0);
error_reporting(0);

$server = new Swoole\HTTP\Server("127.0.0.1", $_ENV['PORT']);

$server->on("start", function (Swoole\Http\Server $server) {
    echo "API for solr searching is started at http://127.0.0.1:".$_ENV['PORT']."\n";
});

$server->on("request", function (Swoole\Http\Request $request, Swoole\Http\Response $response) {
    $status = '200';
    if ($request->server['request_uri'] == '/') {
        if ($request->getMethod() == 'GET') {
            $res = array();
            $res['about'] = 'API for Solr - v2';
            $res['version'] = '1.0.0';
        } else {
            $status = '405';
            $res = array();
        }
    } elseif ($request->server['request_uri'] == '/api/v1') {
        if ($request->getMethod() == 'GET') {
            $res['about'] = 'Default search';
            $res['version'] = '1.0.0';
            $ds = new \Fdmi\Api2solr\Models\Search($request);
            $res['data'] = $ds->getRes();
        } else {
            $status = '405';
            $res = array();
        }

    } else {
        $status = '404';
    }

    new \Fdmi\Api2solr\Views\ViewJson($response, $res, $status);

});

$server->start();